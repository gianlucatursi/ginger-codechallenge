(function(){

	'use strict';

	var controllers = angular.module('ginger.controllers', []);

	MainController.$inject = ['$scope','GingerClient'];
	controllers.controller('MainController', MainController);
	
	/**
	 * MainController 
	 * @param {Object} $scope       [description]
	 * @param {GingerClient} GingerClient is a service for call json-server
	 */
	function MainController($scope, GingerClient){

		var _this = this;
		_this.scope = $scope;

		_this.callbackButton = _callBackButton;
		_this.promise = _promiseButton;
		_this.addPayment = _addPayment;

		_this.filter = { Method: ''};
		_this.newPayment = {};
		_this.payments = [];
		////////////////// FUNCTIONS //////////////////
		/**
		 * Callback for <Callback button>
		 * @return {[type]} [description]
		 */
		function _callBackButton(){
			GingerClient
				.get({
					_limit: 20,
					_sort: "amount",
					_order: "DESC"
				}).then(
					function _success( response ){
						_this.payments = addClassPayment(response.data);
						_this.scope.$apply();
					},
					function _fail( error ){
						alert(JSON.stringify(error));
					} 
				);
		}

		/**
		 * Callback for Promise button
		 * @return {[type]} [description]
		 */
		function _promiseButton(){
			GingerClient
				.get({
					merchant : 'Ginger'
				}).then(
					function _success( response ){
						_this.payments = addClassPayment(response.data);
						_this.scope.$apply();
					},
					function _fail( error ){
						alert(JSON.stringify(error));
					} 
				);
		}

		/**
		 * private
		 */
		function addClassPayment(data){
			var result = [];
			_.each(data, 
				function(elem){
					if(elem.status == 'accepted')
						elem.class = 'success';
					else
						elem.class = 'danger';
					result.push(elem);
				});

			return result;
		}

		/**
		 * Used to add payment
		 */
		function _addPayment(){

			_this.newPayment.currency = "EUR";
			_this.newPayment.status = "accepted";
			_this.newPayment.created = new Date();

			GingerClient
				.post(_this.newPayment)
				.then(
					function _success( response ){
						_this.newPayment = {};
						alert('Success!');
						$('#addPayment').modal('hide');
					},
					function _fail( error ){
						_this.newPayment = {};
						alert('OPS!');
						$('#addPayment').modal('hide');
					} 
				)

		}
	}

})(window.angular);