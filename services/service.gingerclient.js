(function(){

	'use strict';

	var services = angular.module('ginger.services', [])
	
	services.constant("Enviroment", {
        url: "http://localhost:3000",
        database: "/payments"
    });

	GingerClient.$inject = ['$q', '$http', 'Enviroment'];
	services.service('GingerClient', GingerClient);

	/**
	 * GingerClient it's used for calling JSON-Server
	 */
	function GingerClient($q, $http, Enviroment){

		return {
			get : _get,
			post : _post
		}

		/**
		 * Call the json server with /GET
		 * @param  {} options query
		 * @return {Promise}
		 */
		function _get(options){
			var url = Enviroment.url + Enviroment.database;
			if(!_.isEmpty(options)){
				url += "?";
				_.each(options, function(value, key){
					url += "&" + key + "="+ value;
				});
			}

			return $http.get(url);
		}

		/**
		 * Call the json server with /POST
		 * @param  {} options data
		 * @return {Promise}
		 */
		function _post(object){

			var url = Enviroment.url + Enviroment.database;
			return $http.post(url, object);

		}


	}

})(window.angular);